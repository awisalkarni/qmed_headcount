from datetime import datetime #import datetime to get the date and time
import math
import cv2
import numpy as np
import logging
import argparse
import requests
import json


#global variables
width = 0
height = 0
EntranceCounter = 0
ExitCounter = 0
LineWidth = 5
MinContourArea = 5000  #Adjust ths value according to your usage
MovementTolerance = 100
BinarizationThreshold = 40  #Adjust ths value according to your usage
ReferenceFrame = None #ReferenceFrame
timeNow = datetime.now()
dataFolder = 'Data/'
host = 'https://qmed.asia'
SessionId = 1
crop = 0
cropX1 = 0
cropX2 = 300
cropY1 = 0
cropY2 = 300

# logging.basicConfig(filename=dataFolder + timeNow.strftime('%Y-%m-%d_%H:%M:%S') + "_logfile.log", format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %I:%M:%S %p', level=logging.INFO)

# logging.info("Created " + timeNow.strftime('%Y-%m-%d_%H:%M:%S') + "-HeadCount.csv")
# logging.info("Type, Number")
# logging.info("Entrances, 0")
# logging.info("Exits, 0")

def ProcessGrayFrame(frame):
    #gray-scale convertion and Gaussian blur filter applying
    GrayFrame = cv2.cvtColor(Frame, cv2.COLOR_BGR2GRAY)
    GausFrame = cv2.GaussianBlur(GrayFrame, (21, 21), 0)
    return GausFrame

def ProcessThreshFrame(frame):
    GrayFrame = ProcessGrayFrame(frame)
    #Background subtraction and image binarization
    FrameDelta = cv2.absdiff(ReferenceFrame, GrayFrame)
    FrameThresh = cv2.threshold(FrameDelta, BinarizationThreshold, 255, cv2.THRESH_BINARY)[1]
    FrameDilate = cv2.dilate(FrameThresh, None, iterations=2)#Dilate image and find all the contours
    return FrameDilate

# This function compares 2 contours to see if they could possibly be the same contours
def ContourCompare(contour1, contour2):
    (x, y) = contour2[0]
    (cx, cy) = contour1[0]
    distance = math.sqrt((x-cx)**2+(y-cy)**2)
    #print(str(distance))
    return distance < MovementTolerance

# This function compares 1 coutour with a list and returns the first match
def ContourTrack(contour, contourlist):
    if len(contourlist) == 0: # if the list is empty, then return -1
        return -1

    #check all the items for a match
    for x in range(0, len(contourlist)):
        if ContourCompare(contour, contourlist[x]):
            return x

    return -1

def UpdateBackend(host, count, sessionId, type):
    payload = json.dumps({"count": count,"session_id": sessionId,"count_type": type})
    headers = {'Accept': 'application/json','Content-Type': 'application/json'}
    response = requests.request("POST", host+'/api/update', headers=headers, data=payload)

    return response.text

# args
parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
#parser.add_argument('-m', '--model', type=str, required=True, help='File path of .tflite file.')
#parser.add_argument('-l', '--labelmap', type=str, required=True, help='File path of labels file.')
parser.add_argument('-i', '--in_count', type=int, default=0, help='Initial in counts')
parser.add_argument('-o', '--out_count', type=int, default=0, help='Initial out count')
parser.add_argument('-c', '--camera', type=str, default="rtsp://192.168.0.164", help='Camera position.')
parser.add_argument('-u', '--host', type=str, default='https://qmed.asia', help='Backend host url')
parser.add_argument('-s', '--session_id', type=int, default=1, help='Session ID from backend')
parser.add_argument('-p', '--crop', type=int, default=0, help='crop image from top left 300x300')
#parser.add_argument('-roi', '--roi_position', type=float, default=0.6, help='ROI Position (0-1)')
#parser.add_argument('-la', '--labels', nargs='+', type=str,help='Label names to detect (default="all-labels")')
#parser.add_argument('-a', '--axis', default=True, action="store_false", help='Axis for cumulative counting (default=x axis)')
#parser.add_argument('-e', '--use_edgetpu', action='store_true', default=False, help='Use EdgeTPU')
#parser.add_argument('-s', '--skip_frames', type=int, default=20, help='Number of frames to skip between using object detection model')
#parser.add_argument('-sh', '--show', default=True, action="store_false", help='Show output')
#parser.add_argument('-sp', '--save_path', type=str, default='', help='Path to save the output. If None output won\'t be saved')
#parser.add_argument('--type', choices=['tensorflow', 'yolo', 'yolov3-tiny'], default='tensorflow', help='Whether the original model was a Tensorflow or YOLO model')
args = parser.parse_args()

crop = args.crop

EntranceCounter = args.in_count
ExitCounter = args.out_count
host = args.host
SessionId = args.session_id

#start capturing footage
# cv2.namedWindow("preview")
camera = cv2.VideoCapture(args.camera)

#force 640x480 webcam resolution

resX = 640
resY = 480
camera.set(3,resX)
camera.set(4,resY)
if crop == 0: 
    BinaryFrame1 = np.zeros((resY, resX), dtype=np.uint8)
else: 
    BinaryFrame1 = np.zeros((cropX2, cropY2), dtype=np.uint8)

ContourData = []
ActiveContours = []

#The webcam maybe get some time / captured frames to adapt to ambience lighting. For this reason, some frames are grabbed and discarted.
for i in range(0,20):
    (grabbed, Frame) = camera.read()
    
    if crop == 1:
        Frame = Frame[cropY1:cropY2, cropX1:cropX2]

    ReferenceFrame = ProcessGrayFrame(Frame)

#encapsulate infinite while block inside a try statement
try:
    while True:
        (grabbed, Frame) = camera.read()

        if crop == 1:
            Frame = Frame[cropY1:cropY2, cropX1:cropX2]

        timeNow = datetime.now()

        height = np.size(Frame,0)
        width = np.size(Frame,1)

        #if cannot grab a frame, this program ends here.
        if not grabbed:
            break

        

        if crop == 1:
            Frame = Frame[cropY1:cropY2, cropX1:cropX2]

        BinaryFrame2 = ProcessThreshFrame(Frame)

        BinaryFrameCombo = cv2.bitwise_or(BinaryFrame1, BinaryFrame2)

        # combine the last two frames and fine the contours
        _, cnts, _ = cv2.findContours(BinaryFrameCombo, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        BinaryFrame1 = BinaryFrame2

        #plot reference lines (entrance and exit lines)
        cv2.line(Frame, (0,int(height/2)), (int(width),int(height/2)), (255, 0, 0), LineWidth)


        #check all found countours
        for c in cnts:
            #if a contour has small area, it'll be ignored
            if cv2.contourArea(c) < MinContourArea:
                continue

            #draw an rectangle "around" the object
            (x, y, w, h) = cv2.boundingRect(c)
            rectBounds = (x, y, w, h)
            cv2.rectangle(Frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            #find object's centroid
            CoordXCentroid = (x+x+w)/2
            CoordYCentroid = (y+y+h)/2
            ObjectCentroid = (int(CoordXCentroid),int(CoordYCentroid))
            cv2.circle(Frame, ObjectCentroid, 1, (0, 0, 0), 5)
            cv2.circle(Frame, ObjectCentroid, MovementTolerance, (255,255,255), 5)

            crossedBoundary = False

            #create new tuple of object
            ContourTuple = (ObjectCentroid, crossedBoundary)

            #compare to list
            cResult = ContourTrack(ContourTuple, ContourData)
            print(ContourData)

            if cResult is -1:
                ActiveContours.append(ContourTuple)
            else: # replace the previous contour data with the new contour data
                #Check to see if you crossed the line in the middle
                if ContourData[cResult][1]: #tracked object has already crossed the line
                    ContourTuple = (ContourTuple[0], True)
                elif (ContourData[cResult][0][1] >= height/2 and ContourTuple[0][1] <= height/2):
                    if not ContourData[cResult][1]:
                        EntranceCounter = EntranceCounter + 1
                        ContourTuple = (ContourTuple[0], True)
                        logging.info("Entrances, " + str(EntranceCounter))
                        UpdateBackend(host, 1, SessionId, "in")
                elif (ContourData[cResult][0][1] <= height/2 and ContourTuple[0][1] >= height/2):
                    if not ContourData[cResult][1]:
                        ExitCounter = ExitCounter + 1
                        ContourTuple = (ContourTuple[0], True)
                        logging.info("Exits, " + str(ExitCounter))
                        UpdateBackend(host, 1, SessionId, "out")

                ActiveContours.append(ContourTuple)


        ContourData = ActiveContours
        ActiveContours = []

        #Write entrance and exit counter values on frame and shows it
        cv2.putText(Frame, "Entrances: {}".format(str(EntranceCounter)), (10, 50),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (250, 0, 1), 2)
        cv2.putText(Frame, "Exits: {}".format(str(ExitCounter)), (10, 70),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

        cv2.imshow("Original Frame", Frame)

        cv2.waitKey(1);

except KeyboardInterrupt:
    # cleanup the camera and close any open windows
    camera.release()
    cv2.destroyAllWindows()
